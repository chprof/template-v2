module.exports = [
	{
		name: 'typography',
		ignore: false,
		template: {
			html: './components/typography/typography.html',
			scss: './components/typography/_typography.scss'
		},
	},
	{
		name: 'title',
		ignore: false,
		template: {
			html: './components/title/title.html',
			scss: './components/title/_title.scss'
		},
	},
	{
		name: 'text',
		ignore: false,
		template: {
			html: './components/text/text.html',
			scss: './components/text/_text.scss'
		},
	},
	{
		name: 'button',
		ignore: false,
		template: {
			html: './components/button/button.html',
			scss: './components/button/_button.scss'
		},
	},
	{
		name: 'link',
		ignore: false,
		template: {
			html: './components/link/link.html',
			scss: './components/link/_link.scss'
		},
	},
	{
		name: 'logo',
		ignore: false,
		template: {
			html: './components/logo/logo.html',
			scss: './components/logo/_logo.scss'
		},
	},
	{
		name: 'carousel',
		ignore: false,
		template: {
			html: './components/carousel/carousel.html',
			scss: './components/carousel/_carousel.scss',
			js: './components/carousel/carousel.js'
		},
	},
]