const fontSizeRelative = 14,
	$xs = 0,
	$sm = 576,
	$md = 768,
	$lg = 992,
	$xl = 1200,
	$xxl = 1400;

	
module.exports = [
	{
		name: 'media',
		options: [
			{ '$xs': 0 },
			{ '$sm': 576 },
			{ '$md': 768 },
			{ '$lg': 992 },
			{ '$xl': 1200 },
			{ '$xxl': 1400 },
		]
	},
	{
		name: 'global',
		options: [
			{'$font': '\'SFProDisplay\''},
			{'$color': 'rgba(40, 47, 54, 1)'},
			{'$linkColor': 'rgba(69, 146, 255, 1)'},
			{'$backgroundColor': 'rgba(224, 224, 224, 1)'},
			{'$borderColor': 'rgba(224, 224, 224, 1)'},
			{'$fontSizeRelative': fontSizeRelative},
			{'$fontSize': '1rem'},
			{'$isBootstrap': true},
		]
	},
	{
		name: 'optionsConfig',
		options: [
			{
				name: 'grid',
				selector: "container",
				options: [
					{
						selector: '',
						value: '1400px',
						gutters: ''
					},
				]
			},
			{
				name: 'font-family',
				selector: "ff",
				options: [
					{
						name: 'SFProDisplay',
						src: '../fonts/SFProDisplay/SFProDisplay',
						options: [
							{
								weight: 100,
								style: 'normal'
							},
							{
								weight: 100,
								style: 'italic'
							},
							{
								weight: 400,
								style: 'normal'
							},
							{
								weight: 400,
								style: 'italic'
							},
						]
					}
				]
			},
			{
				name: 'font-size-root',
				selector: "fzr",
				options: [
					{
						mediaString: 'xs',
						media: $xs,
						value: '10px'
					},
					{
						mediaString: 'md',
						media: $md,
						value: '12px'
					},
					{
						mediaString: 'xxl',
						media: $xxl,
						value: '14px'
					},
				]
			},
			{
				name: 'font-size-title',
				selector: "h",
				options: [
					{ '1': 54 / fontSizeRelative + 'rem' },
					{ '2': 40 / fontSizeRelative + 'rem' },
					{ '3': 28 / fontSizeRelative + 'rem' },
					{ '4': 20 / fontSizeRelative + 'rem' },
					{ '5': 18 / fontSizeRelative + 'rem' },
					{ '6': 14 / fontSizeRelative + 'rem' }
				]
			},
			{
				name: 'font-size-text',
				selector: "text",
				options: [
					{ '0': 27 / fontSizeRelative + 'rem' },
					{ '1': 24 / fontSizeRelative + 'rem' },
					{ '2': 20 / fontSizeRelative + 'rem' },
					{ '3': 18 / fontSizeRelative + 'rem' },
					{ '4': 16 / fontSizeRelative + 'rem' },
					{ '5': 15 / fontSizeRelative + 'rem' },
					{ '6': 14 / fontSizeRelative + 'rem' },
					{ '7': 12 / fontSizeRelative + 'rem' },
					{ '8': 10 / fontSizeRelative + 'rem' },
					{ '9': 9 / fontSizeRelative + 'rem' },
					{ '10': 8 / fontSizeRelative + 'rem' },
				]
			},
			{
				name: 'border-radius',
				selector: "bdrs",
				options: [
					{ '1': 25 / fontSizeRelative + 'rem' },
					{ '2': 20 / fontSizeRelative + 'rem' },
					{ '3': 10 / fontSizeRelative + 'rem' },
					{ '4': 5 / fontSizeRelative + 'rem' },
					{ '5': 3 / fontSizeRelative + 'rem' },
					{ '6': 0 / fontSizeRelative + 'rem' },
				]
			},
			{
				name: 'colors',
				selector: "cf",
				options: [
					{
						name: 'dark',
						selector: "1",
						options: [
							{ '0': 'rgba(40, 47, 54, 1)' },
							{ '1': 'rgba(40, 47, 54, 0.8)' },
							{ '2': 'rgba(40, 47, 54, 0.7)' },
							{ '3': 'rgba(40, 47, 54, 0.5)' },
							{ '4': 'rgba(40, 47, 54, 0.3)' },
							{ '5': 'rgba(40, 47, 54, 0.15)' },
							{ '6': 'rgba(40, 47, 54, 0.08)' },
							{ '7': 'rgba(40, 47, 54, 0.04)' },
							{ '8': 'rgba(63, 62, 62, 1)' },
							{ '9': 'rgba(63, 62, 62, 0.5)' },
							{ '10': 'rgba(30, 36, 43, 1)' },
							{ '11': 'rgba(64, 72, 83, 1)' },
							{ '12': 'rgba(35, 36, 36, 1)' },
						]
					},
					{
						name: 'gray',
						selector: "2",
						options: [
							{ '0': 'rgba(51, 51, 51, 1)' },
							{ '1': 'rgba(79, 79, 79, 1)' },
							{ '2': 'rgba(130, 130, 130, 1)' },
							{ '3': 'rgba(189, 189, 189, 1)' },
							{ '4': 'rgba(224, 224, 224, 1)' },
							{ '5': 'rgba(64, 72, 82, 1)' },
							{ '6': 'rgba(80, 80, 80, 1)' },
							{ '7': 'rgba(143, 146, 150, 1)' },
							{ '8': 'rgba(190, 192, 194, 1)' },
							{ '9': 'rgba(204, 213, 230, 1)' },
							{ '10': 'rgba(238, 239, 239, 1)' },
							{ '11': 'rgba(242, 242, 242, 1)' },
						]
					},
					{
						name: 'grayblue',
						selector: "3",
						options: [
							{ '0': 'rgba(249, 249, 249, 1)' },
						]
					},
					{
						name: 'white',
						selector: "4",
						options: [
							{ '0': 'rgba(255, 255, 255, 1)' },
							{ '1': 'rgba(255, 255, 255, 0.8)' },
							{ '2': 'rgba(255, 253, 253, 0.8)' },
						]
					},
					{
						name: 'red',
						selector: "5",
						options: [
							{ '0': 'rgba(235, 87, 87, 1)' },
							{ '1': 'rgba(255, 79, 82, 1)' },
							{ '2': 'rgba(219, 68, 55, 1)' },
							{ '3': 'rgba(255, 69, 69, 1)' },
						]
					},
					{
						name: 'orange',
						selector: "6",
						options: [
							{ '0': 'rgba(242, 153, 74, 1)' },
							{ '1': 'rgba(255, 163, 77, 1)' },
						]
					},
					{
						name: 'yellow',
						selector: "7",
						options: [
							{ '0': 'rgba(242, 201, 76, 1)' },
						]
					},
					{
						name: 'green',
						selector: "8",
						options: [
							{ '0': 'rgba(33, 150, 83, 1)' },
							{ '1': 'rgba(39, 174, 96, 1)' },
							{ '2': 'rgba(111, 207, 151, 1)' },
							{ '3': 'rgba(69, 210, 137, 1)' },
							{ '4': 'rgba(61, 196, 126, 1)' },
						]
					},
					{
						name: 'blue',
						selector: "9",
						options: [
							{ '0': 'rgba(24, 119, 242, 1)' },
							{ '1': 'rgba(45, 156, 219, 1)' },
							{ '2': 'rgba(86, 204, 242, 1)' },
							{ '3': 'rgba(69, 146, 255, 1)' },
							{ '4': 'rgba(0, 160, 220, 1)' },
							{ '5': 'rgba(29, 161, 242, 1)' },
							{ '6': 'rgba(37, 83, 180, 1)' },
							{ '7': 'rgba(47, 128, 237, 1)' },
						]
					},
					{
						name: 'purple',
						selector: "10",
						options: [
							{ '0': 'rgba(155, 81, 224, 1)' },
							{ '1': 'rgba(187, 107, 217, 1)' },
						]
					}
				]
			},
			{
				name: 'background-color',
				selector: "bgc",
				options: [
					{
						name: 'gray',
						selector: "1",
						options: [
							{'0': 'rgba(238, 239, 239, 1)'},
							{'1': 'rgba(242, 242, 242, 1)'},
							{'2': 'rgba(40, 47, 54, 0.04)'},
							{'3': 'rgba(147, 151, 155, 1)'},
							{'4': 'rgba(238, 239, 239, 1)'},
						]
					},
					{
						name: 'white',
						selector: "2",
						options: [
							{'0': 'rgba(255, 255, 255, 1)'},
						]
					},
					{
						name: 'blue',
						selector: "3",
						options: [
							{'0': 'rgba(69, 146, 255, 1)'},
							{'1': 'rgba(197, 228, 252, 0.5)'},
						]
					},
					{
						name: 'green',
						selector: "4",
						options: [
							{'0': 'rgba(61, 196, 126, 1)'},
						]
					},
					{
						name: 'dark',
						selector: "5",
						options: [
							{'0': 'rgba(0, 0, 0, 0.5)'},
						]
					},
				]
			},
			{
				name: 'border-color',
				selector: "bdc",
				options: [
					{
						name: 'gray',
						selector: "1",
						options: [
							{'0': 'rgba(40, 47, 54, 0.15)'},
							{'1': 'rgba(224, 224, 224, 1)'},
						]
					},
					{
						name: 'blue',
						selector: "2",
						options: [
							{'0': 'rgba(69, 146, 255, 1)'},
							{'1': 'rgba(162, 210, 252, 1)'},
						]
					},
					
				]
			},
			{
				name: 'box-shadow-color',
				selector: "bxsh",
				options: [
					{
						name: 'dark',
						selector: "1",
						options: [
							{'0': 'rgba(0, 0, 0, 0.06)'},
						]
					},
				]
			},
		]
	}
];