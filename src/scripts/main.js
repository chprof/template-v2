const { includes } = require("../../cli/vars");

document.addEventListener("DOMContentLoaded", () => {
  svg4everybody();
	var animateCSS = function(element, animation, time, prefix = 'animate__') {
		return new Promise((resolve, reject) => {
			var animationName = `${prefix}${animation}`;
			var node = document.querySelector(element);
			
			node.classList.add(`${prefix}animated`, animationName);

			function handleAnimationEnd(event) {
				event.stopPropagation();
				setTimeout(function() {
					node.classList.remove(`${prefix}animated`, animationName);
					resolve('Animation ended');
				}, time)
			}

			node.addEventListener('animationend', handleAnimationEnd, {once: true});
		});
	};
  (function() {
    var forms = document.querySelectorAll('.needs-validation');  
    validation = Array.prototype.filter.call(forms, function(form) {
      
      form.addEventListener('submit', function(event) {
        if (form.checkValidity() === false) {
          event.preventDefault();
          event.stopPropagation();
        }
        form.classList.add('was-validated');
      }, false);
    });
  })();

	@@include('./components.js')
});